var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
var mongoUrl = process.env.MONGO_URL || "mongodb://localhost:27017/mongodemo";
//'mongodb://test:test1234@ds018538.mlab.com:18538/mongodemo'
mongoose.connect(mongoUrl);

module.exports = {mongoose};
